define(function(require){
	var	config 			= require('config');
	var Loader 			= require('loader');
	var Ticker 			= require('ticker');
	var Utils 			= require('utils');
	var Resizer			= require('resizer');
	var UI 	    		= require('ui');
	var ReelsContainer	= require('reelsContainer');
	var SoundManager    = require('soundManager');

	var Main = function() {
		this.stage = new PIXI.Container();
		this.renderer = PIXI.autoDetectRenderer(config.gameSize.width, config.gameSize.height, { resolution: config.gameResolution, roundPixels: config.roundPixels, antialias: config.antialias, backgroundColor: config.canvasColor });		
		this.resizer = new Resizer(this.renderer.view);
		this.loadAssets();
	};

	Main.prototype.loadAssets = function() {
		this.loader = new Loader();
			
		this.loader.onComplete.add(function() {
			this.initComponents();			
			this.startRendering();
		}.bind(this));

		this.loader.loadAssets();
	};

	Main.prototype.initComponents = function() {
		this.UI = new UI();
		this.reelsContainer = new ReelsContainer(this.UI);
		this.soundManager = new SoundManager();
		this.stage.addChild(this.reelsContainer);
		this.stage.addChild(this.UI);
	};

	Main.prototype.startRendering = function() {
		document.body.appendChild(this.renderer.view);

		Ticker.add(function() {
			this.renderer.render(this.stage);
		}.bind(this));
	};

	new Main();	
});