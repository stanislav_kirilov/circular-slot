define(function() {
	return {
		gameSize: {
			width: 1280,
			height: 720
		},
		gameResolution: 2,
		roundPixels: true,
		antialias: true,
		canvasColor: 0X69C8C4,	
		reelsCount: 5,
		reel: {
			width: 31,
			initialRadius: 190,
			colors: [0x5890DC, 0XEC5783, 0X7BE0C3, 0XF6E652, 0XAD33D8],
			spinDuration: 9,
			delayBetweenReelSpin: 0.1,
			rotationRanges: [{ min: 160, max: 224 }, { min: 192, max: 288 }, { min: 224, max: 352 }, { min: 256, max: 416 }, { min: 288, max: 480 }],
			borderWidths: [1, 1, 1, 1, 3],
			border: {
				color: 0x000000,
				fadeInAlpha: 1,
				fadeOutAlpha: 0.1,
				fadeDuration: 1
			},
			symbols: {
				count: [40, 44, 52, 60, 64],
				scale: 0.2,
				anchorX: 0.5,
				anchorsY: [7.93, 9.14, 10.33, 11.57, 12.78],
				blurOnSpin: 2,
				addBlurProgressValue: 0.30,
				removeBlurProgressValue: 0.9,
				fadeInAlpha: 1,
				fadeOutAlpha: 0.1,
				fadeDuration: 1,
				sprites: ["cherry.png", "chip.png", "cocktail.png", "crown.png", "grape.png", "lemon.png", "strawberry.png", "cups.png", "watermelon.png"]
			},
			center: {
				color: 0Xf6e652,
				alpha: 1,
				border: {
					width: 3,
					color: 0x000000
				}
			}
		},
		reelsZoom: {
			zoomInDuration: 1,
			zoomOutDuration: 1,
			delayBeforeZoomIn: 1000,
			delayBeforeSpinActivation: 500,
			delayBeforeStopActivation: 2000,
			delayBeforeZoomReset: 2000,
			scale: 6,
			x: -1690,
			y: -1800
		},
		interface: {
			buttons: {
				spinButton: {
					text: "SPIN",
					color: 0XAD33D8,
					size: 60,
					anchor: 0.5,
					textStyle: {fontFamily : "Verdana", fontWeight: "bold", fontSize: 21, fill : 0XFFFFFF, align : "center"},
					textAnchor: 0.5,
					border: {
						width: 3,
						color: 0x000000,
						size: 61
					},
					position: {
						x: 640,
						y: 360
					},
					states: {
						normal: 0XFFFFFF,
						hover: 0xDDDDDD,
						pressDown: 0XAAAAAA,
						pressUp: 0XFFFFFF,
						inactive: 0x777777
					}
				},
				stopButton: {
					text: "STOP",
					color: 0XFF6550,
					size: 60,
					anchor: 0.5,
					textStyle: {fontFamily : "Verdana", fontWeight: "bold", fontSize: 21, fill : 0XFFFFFF, align : "center"},
					textAnchor: 0.5,
					border: {
						width: 3,
						color: 0x000000,
						size: 61
					},
					position: {
						x: 640,
						y: 360
					},
					states: {
						normal: 0XFFFFFF,
						hover: 0xDDDDDD,
						pressDown: 0XAAAAAA,
						pressUp: 0XFFFFFF,
						inactive: 0x777777
					}
				},
				soundButton: {
					text: "MUTE",
					toggleText: "UNMUTE",
					color: 0XEC5783,
					size: 30,
					anchor: 0.5,
					textStyle: {fontFamily : "Verdana", fontWeight: "bold", fontSize: 10, fill : 0XFFFFFF, align : "center"},
					textAnchor: 0.5,
					border: {
						width: 3,
						color: 0x000000,
						size: 30
					},
					position: {
						x: 40,
						y: 680
					},
					states: {
						normal: 0XFFFFFF,
						hover: 0xDDDDDD,
						pressDown: 0XAAAAAA,
						pressUp: 0XFFFFFF,
						inactive: 0x777777
					}
				}
			},			
			grid: {
				sprite: "grid.png",
				position: {
					x: 310,
					y: 360
				},
				anchorY: 0.5,
				scale: 0.395
			},
			logo: {
				sprite: "logo.png",
				position: {
					x: 5,
					y: 10
				},
				scale: 0.29
			},
			author: {
				text: "Author: Stanislav Kirilov",
				textStyle: {fontFamily : "Trebuchet MS", fontWeight: "bold", fontSize: 14, fill : 0XFFFFFF, align : "center"},
				position: {
					x: 12,
					y: 60
				}
			}
		},
		assets: [
			"img/assets.json"
		],
		sounds: {
			configs: [
				{
					name: "backgroundSound",
					src: "sounds/backgroundSound.mp3",
					rate: 1,
					loop: true,
					volume: 0.7,
					autoplay: true
				},
				{
					name: "reelSpin",
					src: "sounds/reelSpin.mp3",
					rate: 2,
					loop: false,
					volume: 1,
					autoplay: false,
					playSoundAfterDelay: {
						name: "reelLoop",
						delay: 3500
					}
				},
				{
					name: "reelLoop",
					src: "sounds/reelLoop.mp3",
					rate: 2,
					loop: true,
					volume: 1,
					autoplay: false,
					playSoundAfterDelay: {
						name: "reelStop",
						delay: 3500
					}
				},
				{
					name: "reelStop",
					src: "sounds/reelStop.mp3",
					rate: 2.8,
					loop: false,
					volume: 1,
					autoplay: false
				},
				{
					name: "reelForceStop",
					src: "sounds/reelForceStop.mp3",
					rate: 1,
					loop: false,
					volume: 1,
					autoplay: false
				},
				{
					name: "click",
					src: "sounds/click.mp3",
					rate: 1,
					loop: false,
					volume: 1,
					autoplay: false
				}
			],
			spinSoundFadeStart: 0,
			spinSoundFadeEnd: 1,
			spinSoundFadeDuration: 1000
		},		
		particles: {
			showDuration: 1,
			hideDuration: 2,
			updateDelta: 0.018,
			fireParticles: {
				particleImages: ["fire_particle.png"],
				config: {					
			        alpha: {
			        	start: 1,
			        	end: 1
			        },
			        scale: {
			        	start: 0.40,
			        	end: 0,
			        	minimumScaleMultiplier: 1
			        },
			        color: {
			        	start: "#ff9100",
			        	end: "#ff9100"
			        },
			        speed: {
			        	start: 1000,
			        	end: 1000,
			        	minimumSpeedMultiplier: 1
			        },
			        acceleration: {
			        	x: 0,
			        	y: 0
			        },
			        maxSpeed: 0,
			        startRotation: {
			        	min: 0,
			        	max: 360
			        },
			        noRotation: false,
			        rotationSpeed: {
			        	min: 0,
			        	max: 20
			        },
			        lifetime: {
			        	min: 0.25,
			        	max: 0.7
			        },
			        blendMode: "normal",
			        frequency: 0.001,
			        emitterLifetime: -1,
			        maxParticles: 1000,
			        pos: {
			        	x: 640,
			        	y: 360
			        },
			        addAtBack: false,
			        spawnType: "point"
			    }
			},
			smokeParticles: {
				particleImages: ["smoke_particle.png"],				
				config: {
			        alpha: {
			        	start: 0.45,
			        	end: 0
			        },
			        scale: {
			        	start: 0.1,
			        	end: 1,
			        	minimumScaleMultiplier: 1
			        },
			        color: {
			        	start: "#85888d",
			        	end: "#100f0c"
			        },
			        speed: {
			        	start: 300,
			        	end: 400,
			        	minimumSpeedMultiplier: 1
			        },
			        acceleration: {
			        	x: 0,
			        	y: 0
			        },
			        maxSpeed: 0,
			        startRotation: {
			        	min: 0,
			        	max: 360
			        },
			        noRotation: false,
			        rotationSpeed: {
			        	min: 0,
			        	max: 0
			        },
			        lifetime: {
			        	min: 0.5,
			        	max: 1.5
			        },
			        blendMode: "normal",
			        frequency: 0.001,
			        emitterLifetime: -1,
			        maxParticles: 1000,
			        pos: {
			        	x: 640,
			        	y: 360
			        },
			        addAtBack: true,
			        spawnType: "point"
			    }
			}
			
		}
	}
});