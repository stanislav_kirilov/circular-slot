define(function(require) {
	var config 	= require('config');
	var Ticker  = require('ticker');

	var Particles = function() {
		PIXI.Container.call(this);
		this.visible = false;
		this.toggleTween = new TimelineMax();
	};

	Particles.prototype = Object.create(PIXI.Container.prototype);

	Particles.prototype.show = function() {
		this.alpha = 0;
		this.visible = true;
		this.createParticlesTextures();
		this.createParticles();
		this.toggleTween.to(this, config.particles.showDuration, { alpha : 1 });
	};

	Particles.prototype.hide = function() {
		if (!this.visible) return;
		
		this.toggleTween.to(this, config.particles.hideDuration, { alpha : 0, onComplete: function() {
			this.visible = false;
			this.alpha = 1;
			this.removeChildren();			
			this.fireParticleEmitter.destroy();
			this.smokeParticleEmitter.destroy();
			Ticker.remove(this.emitterUpdate, this);
		}.bind(this)})
	};

	Particles.prototype.createParticles = function() {
		this.fireParticleEmitter = new PIXI.particles.Emitter(this, this.fireParticlesTextures, config.particles.fireParticles.config);
		this.smokeParticleEmitter = new PIXI.particles.Emitter(this, this.smokeParticlesTextures, config.particles.smokeParticles.config);
		Ticker.add(this.emitterUpdate, this);
	};

	Particles.prototype.createParticlesTextures = function() {
		this.fireParticlesTextures = [];
		this.smokeParticlesTextures = [];

		config.particles.fireParticles.particleImages.forEach(function(image) {
			this.fireParticlesTextures.push(PIXI.Texture.fromFrame(image));
		}.bind(this));

		config.particles.smokeParticles.particleImages.forEach(function(image) {
			this.smokeParticlesTextures.push(PIXI.Texture.fromFrame(image));
		}.bind(this));
	};

	Particles.prototype.emitterUpdate = function() {
		this.fireParticleEmitter.update(config.particles.updateDelta);
		this.smokeParticleEmitter.update(config.particles.updateDelta);
	};

	return Particles;
});