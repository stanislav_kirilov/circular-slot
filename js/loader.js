define(function(require) {
	var config = require('config');

	var Loader = function() {
		PIXI.loaders.Loader.call(this);
	}

	Loader.prototype = Object.create(PIXI.loaders.Loader.prototype);

	Loader.prototype.loadAssets = function() {
		for (asset in config.assets) {
			this.add(asset, config.assets[asset]);
		}
		
		this.load();
	};

	return Loader;
});