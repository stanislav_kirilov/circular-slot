define(function(require) {
	var config 	= require('config');
	var Signal  = require('libs/signals.min');

	var Button = function(name) {
		PIXI.Container.call(this);

		this.config = config.interface.buttons[name];

		this.x = this.config.position.x;
		this.y = this.config.position.y;

		this.background = new PIXI.Graphics();
		this.background.beginFill(this.config.color);
		this.background.drawCircle(0, 0, this.config.size);
		this.background.endFill();
		this.addChild(this.background);

		this.backgroundBorder = new PIXI.Graphics();
		this.backgroundBorder.lineStyle(this.config.border.width, this.config.border.color);
		this.backgroundBorder.drawCircle(0, 0, this.config.border.size);
		this.backgroundBorder.endFill();
		this.addChild(this.backgroundBorder);

		this.buttonText = new PIXI.Text(this.config.text, this.config.textStyle);
		this.buttonText.anchor.set(this.config.textAnchor);
		this.addChild(this.buttonText);

		this.enable();

		this.events = {
			clicked: new Signal()
		};
	};

	Button.prototype = Object.create(PIXI.Container.prototype);

	Button.prototype.mouseover = function() {
		this.background.tint = this.config.states.hover;
	};

	Button.prototype.mouseout = function() {
		this.background.tint = this.config.states.normal;
	};

	Button.prototype.pointerdown = function() {
		this.background.tint = this.config.states.pressDown;
	};

	Button.prototype.pointerup = function() {
		this.background.tint = this.config.states.pressUp;
		this.events.clicked.dispatch();
	};

	Button.prototype.enable = function() {
		this.interactive = true;
		this.buttonMode = true;
		this.background.tint = this.config.states.normal;
	};

	Button.prototype.disable = function() {
		this.interactive = false;
		this.buttonMode = false;
		this.background.tint = this.config.states.inactive;
	};

	Button.prototype.show = function() {
		this.visible = true;
	};

	Button.prototype.hide = function() {
		this.visible = false;
	};

	Button.prototype.setDefaultText = function() {
		this.buttonText.text = this.config.text;
	};

	Button.prototype.setToggleText = function() {
		if (this.config.toggleText) {
			this.buttonText.text = this.config.toggleText;
		}
	};

	return Button;
});