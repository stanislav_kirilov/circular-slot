define(function() {
	var Utils = function() {
		window.Utils = this;
	};

	Utils.prototype.shuffleArray = function(array) {
		var shuffledArray = [];

	    for (var i = array.length - 1; i > 0; i--) {
	        var j = Math.floor(Math.random() * (i + 1));
	        var temp = array[i];
	        array[i] = array[j];
	        array[j] = temp;
	    }

	    return array;
	};

	return new Utils;
});