define(function(require) {
	var config 			= require('config');
	var Reel 			= require('reel');
	var Signal  		= require('libs/signals.min');
	var Particles		= require('particles');

	var ReelsContainer = function(ui) {
		PIXI.Container.call(this);

		this.ui = ui;
		this.particles = new Particles();
		this.addChild(this.particles);
		this.addReels();
		this.addGrid();
		this.addWheelCenter();

		this.zoomReelsScaleTween = new TimelineMax();
		this.zoomReelsPositionTween = new TimelineMax();
		this.gridToggleTween = new TimelineMax();

		this.ui.events.startSpin.add(this.spinReels.bind(this));	
		this.ui.events.forceStop.add(this.forceStopReels.bind(this));		
	};

	ReelsContainer.prototype = Object.create(PIXI.Container.prototype);

	ReelsContainer.prototype.addReels = function() {
		this.reels = [];

		for (var i = 0; i < config.reelsCount; i++) {
			var reel = new Reel(i);
			this.reels.push(reel);
			this.addChild(reel);
		}
	};

	ReelsContainer.prototype.addGrid = function() {
		this.grid = new PIXI.Sprite.fromFrame(config.interface.grid.sprite);
		this.grid.x = config.interface.grid.position.x;
		this.grid.y = config.interface.grid.position.y;
		this.grid.anchor.y = config.interface.grid.anchorY;
		this.grid.scale.set(config.interface.grid.scale);
		this.grid.visible = false;
		this.grid.visible.alpha = 0;
		this.addChild(this.grid);
	};

	ReelsContainer.prototype.addWheelCenter = function() {
		this.center = new PIXI.Graphics();
		this.center.beginFill(config.reel.center.color, config.reel.center.alpha);
		this.center.drawCircle(config.gameSize.width / 2, config.gameSize.height / 2, config.reel.initialRadius - config.reel.width/2);
		this.center.endFill();
		this.addChild(this.center);

		this.centerBorder = new PIXI.Graphics();
		this.centerBorder.lineStyle(config.reel.center.border.width, config.reel.center.border.color);
		this.centerBorder.drawCircle(config.gameSize.width / 2, config.gameSize.height / 2, config.reel.initialRadius - config.reel.width/2);
		this.centerBorder.endFill();
		this.addChild(this.centerBorder);
	};	

	ReelsContainer.prototype.zoomInReels = function() {
		this.ui.hideSpinButton();
		this.ui.hideStopButton();

		this.zoomReelsScaleTween.to(this.scale, config.reelsZoom.zoomInDuration,
			{
				x: config.reelsZoom.scale,
				y: config.reelsZoom.scale
			}
		);

		this.zoomReelsPositionTween.to(this, config.reelsZoom.zoomInDuration,
			{
				x: config.reelsZoom.x,
				y: config.reelsZoom.y
			}
		);

		this.grid.visible = true;
		this.gridToggleTween.to(this.grid, config.reelsZoom.zoomInDuration,
			{
				alpha: 1,
				onComplete: function() {
					setTimeout(function() {
						this.zoomOutReels();											
					}.bind(this), config.reelsZoom.delayBeforeZoomReset);
				}.bind(this)
			}
		);
	};

	ReelsContainer.prototype.zoomOutReels = function() {
		this.zoomReelsScaleTween.to(this.scale, config.reelsZoom.zoomOutDuration,
			{
				x: 1,
				y: 1,
				ease: Circ.easeOut
			}
		);

		this.zoomReelsPositionTween.to(this, config.reelsZoom.zoomOutDuration,
			{
				x: 0,
				y: 0,
				ease: Circ.easeOut
			}
		);

		this.gridToggleTween.to(this.grid, config.reelsZoom.zoomInDuration,
			{
				alpha: 0,
				ease: Circ.easeOut,
				onComplete: function() {
					this.ui.showSpinButton();
					this.ui.deactivateSpinButton();
					setTimeout(function() {
						this.ui.activateSpinButton();
					}.bind(this), config.reelsZoom.delayBeforeSpinActivation);
					this.grid.visible = false;
				}.bind(this)
			}
		);
	};

	ReelsContainer.prototype.spinReels = function() {
		this.reels.forEach(function(reel) {
			reel.spin();
		});

		SoundManager.playSound("click");
		SoundManager.fade("reelSpin", config.sounds.spinSoundFadeStart, config.sounds.spinSoundFadeEnd, config.sounds.spinSoundFadeDuration)
		SoundManager.playSound("reelSpin");
	};

	ReelsContainer.prototype.forceStopReels = function() {
		this.reels.forEach(function(reel) {
			reel.forceStop();
		});

		SoundManager.playSound("reelForceStop");
		SoundManager.stopSound("reelSpin");
		SoundManager.stopSound("reelLoop");
		SoundManager.stopSound("reelStop");
		SoundManager.clearNextSoundTimeout();

		this.particles.hide();
	};

	return ReelsContainer;
});