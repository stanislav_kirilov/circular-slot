define(function(require) {
	var config 			= require('config');	
	var Button 			= require('button');
	var Signal  		= require('libs/signals.min');	

	var UI = function() {
		PIXI.Container.call(this);

		this.spinButton = new Button("spinButton");
		this.addChild(this.spinButton);

		this.stopButton = new Button("stopButton");
		this.addChild(this.stopButton);

		this.soundButton = new Button("soundButton");
		this.addChild(this.soundButton);

		this.logo = new PIXI.Sprite.fromFrame(config.interface.logo.sprite);
		this.logo.x = config.interface.logo.position.x;
		this.logo.y = config.interface.logo.position.y;
		this.logo.scale.set(config.interface.logo.scale);
		this.addChild(this.logo);

		this.author = new PIXI.Text(config.interface.author.text, config.interface.author.textStyle);
		this.author.x = config.interface.author.position.x;
		this.author.y = config.interface.author.position.y;
		this.addChild(this.author);

		this.events = {
			startSpin: new Signal(),
			forceStop: new Signal()
		};

		this.spinButton.events.clicked.add(function() {
			this.deactivateSpinButton();
			this.events.startSpin.dispatch();
			setTimeout(function() {
				this.showStopButton();
				this.hideSpinButton();
			}.bind(this), config.reelsZoom.delayBeforeStopActivation);
		}.bind(this));

		this.stopButton.events.clicked.add(function() {
			this.deactivateStopButton();
			this.events.forceStop.dispatch();
		}.bind(this));

		this.soundButton.events.clicked.add(function() {
			this.toggleSound();
		}.bind(this));

		this.hideStopButton();
	};

	UI.prototype = Object.create(PIXI.Container.prototype);

	UI.prototype.showSpinButton = function() {
		this.spinButton.show();
	};

	UI.prototype.hideSpinButton = function() {
		this.spinButton.hide();
	};

	UI.prototype.activateSpinButton = function() {
		this.spinButton.show();
		this.spinButton.enable();
	};

	UI.prototype.deactivateSpinButton = function() {
		this.spinButton.show();
		this.spinButton.disable();
	};

	UI.prototype.showStopButton = function() {
		this.stopButton.show();		
		this.activateStopButton();
	};

	UI.prototype.hideStopButton = function() {
		this.stopButton.hide();
	};

	UI.prototype.activateStopButton = function() {
		this.stopButton.show();	
		this.stopButton.enable();
	};

	UI.prototype.deactivateStopButton = function() {
		this.spinButton.show();
		this.stopButton.disable();
	};

	UI.prototype.toggleSound = function() {
		if (SoundManager.isMuted()) {
			this.soundButton.setDefaultText();
			SoundManager.unmute();
		} else {
			this.soundButton.setToggleText();
			SoundManager.mute();
		}
	};

	return UI;
});
