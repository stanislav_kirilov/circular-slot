define(function(require) {
	var config 	= require('config');

	var Reel = function(index) {
		PIXI.Container.call(this);

		this.index = index;		
		this.addBackground();
		this.addSymbols();
		this.symbolsToggleTween = new TimelineMax();
		this.backgroundBorderToggleTween = new TimelineMax();
	};

	Reel.prototype = Object.create(PIXI.Container.prototype);

	Reel.prototype.addBackground = function() {		
		this.background = new PIXI.Graphics();
		this.background.lineStyle(config.reel.width, config.reel.colors[this.index]);
		this.background.drawCircle(config.gameSize.width / 2, config.gameSize.height / 2, config.reel.initialRadius + (this.index * config.reel.width));
		this.background.endFill();
		this.addChild(this.background);

		this.blurFilter = new PIXI.filters.BlurFilter();
		this.blurFilter.blur = config.reel.symbols.blurOnSpin;

		this.backgroundBorder = new PIXI.Graphics();
		this.backgroundBorder.lineStyle(config.reel.borderWidths[this.index], config.reel.border.color);
		this.backgroundBorder.drawCircle(config.gameSize.width / 2, config.gameSize.height / 2, config.reel.initialRadius + (this.index * config.reel.width) + config.reel.width / 2);
		this.backgroundBorder.endFill();
		this.addChild(this.backgroundBorder);
	};

	Reel.prototype.addSymbols = function() {
		this.symbolsContainer = new PIXI.Container();
		this.symbolsContainer.x = config.gameSize.width / 2;
		this.symbolsContainer.y = config.gameSize.height / 2;
		this.addChild(this.symbolsContainer);		

		var symbolsArray = Utils.shuffleArray(config.reel.symbols.sprites);

		for (var i = 0; i < config.reel.symbols.count[this.index]; i++) {
			var symbol = new PIXI.Sprite.fromFrame(symbolsArray[i % config.reel.symbols.sprites.length]);
			symbol.scale.set(config.reel.symbols.scale);
			symbol.anchor.x = config.reel.symbols.anchorX;
			symbol.anchor.y = config.reel.symbols.anchorsY[this.index];
			symbol.rotation = i * ((2 * Math.PI) / config.reel.symbols.count[this.index]);
			this.symbolsContainer.addChild(symbol);
		}
	};

	Reel.prototype.showAnticipation = function() {
		this.background.filters = [this.blurFilter];
		this.symbolsToggleTween.to(this.symbolsContainer, config.reel.symbols.fadeDuration, { alpha: config.reel.symbols.fadeOutAlpha });
		this.backgroundBorderToggleTween.to(this.backgroundBorder, config.reel.border.fadeDuration, { alpha: config.reel.border.fadeOutAlpha });
		if (this.isLastReel()) {
			setTimeout(function() {
				this.parent.particles.show();
			}.bind(this), config.particles.showDuration);
		}
	};

	Reel.prototype.hideAnticipation = function() {
		this.background.filters = [];
		this.symbolsToggleTween.to(this.symbolsContainer, config.reel.symbols.fadeDuration, { alpha: config.reel.symbols.fadeInAlpha });
		this.backgroundBorderToggleTween.to(this.backgroundBorder, config.reel.border.fadeDuration, { alpha: config.reel.border.fadeInAlpha });
		if (this.isLastReel()) {
			setTimeout(function() {
				this.parent.particles.hide();
				this.parent.ui.deactivateStopButton();
			}.bind(this), config.particles.hideDuration);
		}
	};

	Reel.prototype.getRandomRotation = function() {
		return Math.floor(Math.random() * (config.reel.rotationRanges[this.index].max - config.reel.rotationRanges[this.index].min) + config.reel.rotationRanges[this.index].min);
	};

	Reel.prototype.spin = function() {
		this.finalPosition = this.symbolsContainer.rotation - (Math.PI * this.getRandomRotation());

		var spinTween = new TimelineMax();
		spinTween.to(this.symbolsContainer, config.reel.spinDuration,
			{
				rotation: this.finalPosition,
				ease: Circ.easeInOut,
				delay: (config.reelsCount - this.index) * config.reel.delayBetweenReelSpin,
				onUpdate: function() {
					if (spinTween.progress().toFixed(1) == config.reel.symbols.addBlurProgressValue && (!this.background.filters || !this.background.filters.length)) {						
						this.showAnticipation();						
					}
					if (spinTween.progress().toFixed(1) == config.reel.symbols.removeBlurProgressValue && this.background.filters.length) {												
						this.hideAnticipation();
					}
				}.bind(this),
				onComplete: function() {
					if (this.isLastReel()) {
						this.spinCompleteTimeout = setTimeout(function() {
							this.parent.zoomInReels();
						}.bind(this), config.reelsZoom.delayBeforeZoomIn);
					}
				}.bind(this)
			}
		);
	};

	Reel.prototype.forceStop = function() {
		TweenMax.killAll();

		this.background.filters = [];
		this.symbolsContainer.alpha = config.reel.symbols.fadeInAlpha;
		this.backgroundBorder.alpha = config.reel.border.fadeInAlpha;		

		if (this.spinCompleteTimeout) {
			clearTimeout(this.spinCompleteTimeout);
		}
		if (this.zoomInReelsTimeout) {
			clearTimeout(this.zoomInReelsTimeout);
		}
		
		this.symbolsContainer.rotation = this.finalPosition;

		if (this.isLastReel()) {
			this.zoomInReelsTimeout = setTimeout(function() {
				this.parent.zoomInReels();
			}.bind(this), config.reelsZoom.delayBeforeZoomIn);
		}
	};

	Reel.prototype.isLastReel = function() {
		return this.index == config.reelsCount - 1;
	};

	return Reel;
});