define(function(require) {
	var config = require('config');

	var SoundManager = function() {
		this.sounds = {};

		config.sounds.configs.forEach(function(config) {
			var sound = new Howl({
			  src: [config.src],
			  loop: config.loop,
			  volume: config.volume
			});

			sound.volume(config.volume);
			sound.rate(config.rate);
			sound.loop(config.loop);
			sound.playSoundAfterDelay = config.playSoundAfterDelay;

			if (config.autoplay) {
				sound.play();
			}			

			this.sounds[config.name] = sound
		}.bind(this));		

		window.SoundManager = this;
	};

	SoundManager.prototype.playSound = function(name) {
		this.sounds[name].play();
		
		if (this.sounds[name].playSoundAfterDelay) {
  			this.nextSoundPlayTimeout = setTimeout(function() {
  				this.sounds[name].stop();
  				this.playSound(this.sounds[name].playSoundAfterDelay.name);
  			}.bind(this), this.sounds[name].playSoundAfterDelay.delay);
	  	}
	};

	SoundManager.prototype.fade = function(name, from,to, duration) {
		this.sounds[name].fade(from, to, duration);
	};

	SoundManager.prototype.stopSound = function(name) {
		this.sounds[name].stop();
	};

	SoundManager.prototype.mute = function() {
		Howler.volume(0);
	};

	SoundManager.prototype.unmute = function() {
		Howler.volume(1);
	};

	SoundManager.prototype.isMuted = function() {
		return (Howler.volume() == 0);
	};

	SoundManager.prototype.clearNextSoundTimeout = function() {
		if (this.nextSoundPlayTimeout) {
			clearTimeout(this.nextSoundPlayTimeout);
		}
	};

	return SoundManager;
});