define(function(require) {
	var config = require('config');
	
	var Resizer = function(canvas) {
		this.canvas = canvas;
		this.resize();
		window.addEventListener('resize', this.resize.bind(this));
	};

	Resizer.prototype.resize = function() {
		var w = window.innerWidth,
			h = window.innerHeight,
			scale = Math.min(w / config.gameSize.width, h / config.gameSize.height);

		var newWidth = config.gameSize.width * scale,
			newHeight = config.gameSize.height * scale;

		this.canvas.style.position = 'absolute';
		this.canvas.style.width = newWidth + 'px';
		this.canvas.style.height = newHeight + 'px';
		this.canvas.style.left = ((w - newWidth) / 2) + 'px';
		this.canvas.style.top = ((h - newHeight) / 2) + 'px';
	};

	return Resizer;
});