==============================
HOW TO RUN
==============================
1. You need to upload the game to a web server, because the cross origin requests are blocked by default. You can use local web server like XAMP.
2. Open index.html.
3. Enjoy.

==============================
TIME SPENT
==============================
14 hours.

==============================
WHAT I FOCUSED ON
==============================
I have focused on:
1. Implementing all requirements in short amount of time.
2. Adding value to the game. I mainly focused on making the reels to look super fast. I used this video for guideance : https://youtu.be/DiFY2ona3as?t=83	
3. Adding nice and relaxing background music and good, relevant and synced sound for the reels.
4. Making good, easy to maintain and simple code structure.

==============================
WHAT I DIDN`T FOCUSED ON
==============================
Using images. Almost everything is done with code.